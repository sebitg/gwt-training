package com.samsung.gwttraining.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.samsung.gwttraining.shared.pojos.User;

public interface AuthServiceAsync {

	void log(String userName, AsyncCallback<Void> asyncCallback)
			throws IllegalArgumentException;

	// TODO!!!
	// Write method that checks nickname (if it is currently used in system)
	// ASYNC version!

	/**
	 * Check, if nickname is available
	 * 
	 * @param userName
	 */
	void checkNick(String userName, AsyncCallback<Boolean> asyncCallback);
	
	/**
	 * Logs out session user
	 */
	void logOut(AsyncCallback<Boolean> asyncCallback);

	void getUsersOnline(AsyncCallback<List<User>> asyncCallback);
	
	void isSomeoneLogged(AsyncCallback<String> asyncCallback);

}
