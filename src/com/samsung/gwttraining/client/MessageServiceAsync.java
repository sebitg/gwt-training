package com.samsung.gwttraining.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.samsung.gwttraining.shared.pojos.Message;

/**
 * Message service async version
 * @author mody
 *
 */
public interface MessageServiceAsync {

	void sendMessage(Integer toId, Integer fromId, String message, AsyncCallback<Boolean> asyncCallback);
	
	void getMessages(Integer toId, Integer fromId, AsyncCallback<List<Message>> asyncCallback);
	
	void checkNewMessages(Integer toId, Integer fromId, AsyncCallback<Boolean> asyncCallback);
	
}
