package com.samsung.gwttraining.client.events;

public interface Listener {

	void onEvent(Event event);
	
}
