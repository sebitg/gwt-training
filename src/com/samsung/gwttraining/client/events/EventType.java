package com.samsung.gwttraining.client.events;

public enum EventType {

	LOGIN,
	
	LOGOUT,
	
	CLOSE_TAB;
	
}
