package com.samsung.gwttraining.client.events;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public enum EventBus {
	
	INSTANCE;
	
	private Map<EventType, Set<Listener>> events = new LinkedHashMap<>();
	
	public void registerListener(EventType eventType, Listener listener) {
		Set<Listener> set = events.get(eventType);
		if(set==null) {
			set = new HashSet<Listener>();
		}
		set.add(listener);
		events.put(eventType, set);
	}
	
	public void dispatch(EventType eventType, Object data) {
		final Event event = new Event(eventType, data);
		Set<Listener> set = events.get(eventType);
		if(set!=null) {
			for(Listener item: set) {
				item.onEvent(event);
			}
		}
	}
	
	public void clear() {
		events.clear();
	}
}
