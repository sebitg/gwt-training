package com.samsung.gwttraining.client.events;

/**
 * Simple event class
 * @author mody
 *
 */
public class Event {
	
	private EventType eventType;
	
	private Object data;

	public EventType getEventType() {
		return eventType;
	}
	
	public Event(EventType eventType) {
		this.eventType = eventType;
	}
	
	public Event(EventType eventType, Object data) {
		this.eventType = eventType;
		this.data = data;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	

}
