package com.samsung.gwttraining.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.samsung.gwttraining.client.events.Event;
import com.samsung.gwttraining.client.events.EventBus;
import com.samsung.gwttraining.client.events.EventType;
import com.samsung.gwttraining.client.events.Listener;
import com.samsung.gwttraining.client.views.HomeView;
import com.samsung.gwttraining.client.views.LogView;

/**
 * 
 * @author mody
 *
 */
public class Gwttraining implements EntryPoint, Listener {
	
	private static final AuthServiceAsync authService = (AuthServiceAsync)GWT.create(AuthService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		EventBus.INSTANCE.registerListener(EventType.LOGIN, this);
		EventBus.INSTANCE.registerListener(EventType.LOGOUT, this);
		
		authService.isSomeoneLogged(new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error while session checking!");
				setLayout(new LogView());
			}

			@Override
			public void onSuccess(String result) {
				setLayout((result!=null) ? new HomeView(result) : new LogView());
			}
			
		});

		//TODO!!!
		//Prepare log to chat handling. Initialize async service (in LogView ;)), 
		//handle AsyncCallback, and 
		//change displayed panel to this one
	}

	@Override
	public void onEvent(Event event) {
		final EventType eventType = event.getEventType();
		final Object data = event.getData();
		switch(eventType) {
		case LOGIN:
			if(data!=null) {	//Username is not empty. We have new member!
				setLayout(new HomeView(data.toString()));
			}
			break;
		case LOGOUT:
			setLayout(new LogView());
			break;
		default:
			break;
		}
	}
	
	protected void setLayout(Composite layout) {
		RootLayoutPanel.get().clear();
		RootLayoutPanel.get().add(layout);
	}
}
