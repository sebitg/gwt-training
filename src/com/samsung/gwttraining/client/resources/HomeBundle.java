package com.samsung.gwttraining.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface HomeBundle extends ClientBundle {

	@Source("home/gwt.png")
	ImageResource gwtLogo();
	
}
