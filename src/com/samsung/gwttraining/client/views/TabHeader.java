package com.samsung.gwttraining.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.samsung.gwttraining.client.events.EventBus;
import com.samsung.gwttraining.client.events.EventType;

public class TabHeader extends Composite implements WidgetLifecycle {

	private static TabHeaderUiBinder uiBinder = GWT
			.create(TabHeaderUiBinder.class);

	interface TabHeaderUiBinder extends UiBinder<Widget, TabHeader> {
	}
	
	@UiField
	Label tabHeaderLabel;
	
	@UiField
	Anchor tabClose;
	
	private String label;
	private Integer index;

	@UiConstructor
	public TabHeader(String label, Integer index) {
		initWidget(uiBinder.createAndBindUi(this));
		this.label = label;
		this.index = index;
		
		tabHeaderLabel.setText(this.label);
		onCreate();
	}

	@Override
	public void onCreate() {
		tabClose.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				EventBus.INSTANCE.dispatch(EventType.CLOSE_TAB, index);		//Close tab, sending event to HomeView
			}
		});
	}

	@Override
	public void onDestroy() {
	}

	@Override
	public void onResume() {
	}

}
