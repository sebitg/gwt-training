package com.samsung.gwttraining.client.views;

import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.samsung.gwttraining.client.AuthService;
import com.samsung.gwttraining.client.AuthServiceAsync;
import com.samsung.gwttraining.client.events.Event;
import com.samsung.gwttraining.client.events.EventBus;
import com.samsung.gwttraining.client.events.EventType;
import com.samsung.gwttraining.client.events.Listener;
import com.samsung.gwttraining.shared.pojos.User;

public class HomeView extends Composite implements Listener, WidgetLifecycle {
	
	private static final Cell<User> USER_CELL = new AbstractCell<User>(new String[] {}) {

		@Override
		public void render(com.google.gwt.cell.client.Cell.Context context,
				User value, SafeHtmlBuilder sb) {
			sb.appendHtmlConstant(value.getUserName());
		}
	};
	
	private static final ProvidesKey<User> KEY_PROVIDER = new ProvidesKey<User>() {

		@Override
		public Object getKey(User item) {
			return item.getId();
		}
	};
	
	
	private static final AuthServiceAsync authService = (AuthServiceAsync)GWT.create(AuthService.class);

	private static HomeViewUiBinder uiBinder = GWT
			.create(HomeViewUiBinder.class);
	

	interface HomeViewUiBinder extends UiBinder<Widget, HomeView> {
	}
	
	
	@UiField
	Button logout;

	@UiField
	SplitLayoutPanel homeDockPanel;
	
	@UiField
	Label nameLabel;
	
	@UiField
	CellList<User> chatUsers;
	
	@UiField
	TabLayoutPanel chatWindows;
	
	
	
	private String login;	//Storing login data in case of logout
	private ListDataProvider<User> chatUsersDataProvider = new ListDataProvider<User>();
	

	public HomeView() {
		initWidget(uiBinder.createAndBindUi(this));
		onCreate();
	}
	
	public HomeView(String login) {
		initWidget(uiBinder.createAndBindUi(this));
		nameLabel.setText(login);
		
		onCreate();
	}
	
	@UiFactory
	public CellList<User> createPanelCellList() {		
		final SingleSelectionModel<User> selectionModel = new SingleSelectionModel<>();
		selectionModel.addSelectionChangeHandler(new Handler() {

			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				User selectedObject = selectionModel.getSelectedObject();
				
				Window.alert("I've selected " + selectedObject.getId());
			}
		});
		CellList<User> cellList = new CellList<User>(USER_CELL, KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);
		return cellList;
	}

	@Override
	public void onEvent(Event event) {
		final EventType eventType = event.getEventType();
		final Object data = event.getData();
		switch(eventType) {
		case CLOSE_TAB:
			if(data!=null) {			//Removing panel with specified index
				Integer index = (Integer)data;
				WidgetLifecycle widget = (WidgetLifecycle)chatWindows.getWidget(index);
				widget.onDestroy();		//Frees resources
				chatWindows.remove(index);
			}
			else {
				Window.alert("No info about index of tab to remove!");
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onCreate() {
		//TODO: Create Timer to refresh users list
		
		chatUsersDataProvider.addDataDisplay(chatUsers);
		EventBus.INSTANCE.registerListener(EventType.CLOSE_TAB, this);
		logout.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				authService.logOut(new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Error on log out!");
					}

					@Override
					public void onSuccess(Boolean result) {
						EventBus.INSTANCE.dispatch(EventType.LOGOUT, login);
					}
				});
			}
		});
		onResume();
	}
	
	@Override
	public void onDestroy() {
		//TODO: Dispose Timer to refresh users list
	}

	@Override
	public void onResume() {
		authService.getUsersOnline(new AsyncCallback<List<User>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to get online users!");
			}

			@Override
			public void onSuccess(List<User> result) {
				chatUsersDataProvider.getList().clear();
				chatUsersDataProvider.getList().addAll(result);
			}
		});
	}

}
