package com.samsung.gwttraining.client.views;

public interface WidgetLifecycle {
	
	void onCreate();
	
	void onDestroy();
	
	void onResume();

}