package com.samsung.gwttraining.client.views;

import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.samsung.gwttraining.client.MessageService;
import com.samsung.gwttraining.client.MessageServiceAsync;
import com.samsung.gwttraining.shared.pojos.Message;

public class TabView extends Composite implements WidgetLifecycle {
	
	private static final MessageServiceAsync messageService = (MessageServiceAsync)GWT.create(MessageService.class);

	private static TabViewUiBinder uiBinder = GWT.create(TabViewUiBinder.class);

	interface TabViewUiBinder extends UiBinder<Widget, TabView> {
	}
	
	
	@UiField
	VerticalPanel singleTagPanel;
	
	@UiField
	CellList<Message> panelCellList;
	
	@UiField
	TextArea panelTextArea;
	
	@UiField
	Button panelButton;
	
	private Integer fromId = null;
	
	private ListDataProvider<Message> messageListProvider = new ListDataProvider<Message>();
	
	
	@UiConstructor
	public TabView(Integer fromId) {
		initWidget(uiBinder.createAndBindUi(this));
		this.fromId = (fromId==0) ? null : fromId;	//Info about conversation user ID
		onCreate();
	}

	@UiFactory
	public CellList<Message> createPanelCellList() {
		
		//TODO!!!
		//1. Create KeyProvider and AbstractCell for panelCellList instance...
		//2. Create AbstractCell
		//3. Create SelectionChangeListener/SelectionModel 
		
		final Cell<Message> messageCell = new AbstractCell<Message>(new String[] {}) {

			@Override
			public void render(com.google.gwt.cell.client.Cell.Context context,
					Message value, SafeHtmlBuilder sb) {
				sb.appendHtmlConstant(value.getMsg());
			}
		};
		
		final ProvidesKey<Message> keyProvider = new ProvidesKey<Message>() {

			@Override
			public Object getKey(Message item) {
				return item.getId();
			}
		};
		
		final SingleSelectionModel<Message> selectionModel = new SingleSelectionModel<>();
		selectionModel.addSelectionChangeHandler(new Handler() {

			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				Message selectedObject = selectionModel.getSelectedObject();
				
				Window.alert("I've selected " + selectedObject.getId());
			}
			
		});
		
		
		CellList<Message> cellList = new CellList<Message>(messageCell, keyProvider);
		cellList.setSelectionModel(selectionModel);
		messageListProvider.addDataDisplay(cellList);
		return cellList;
	}

	@Override
	public void onCreate() {
		//TODO create Timer here.... Get message list once a 15 seconds
		
		panelButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				messageService.sendMessage(fromId, null, panelTextArea.getText(), new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Failed to send message!");
					}

					@Override
					public void onSuccess(Boolean result) {
						onResume();
					}
				});
			}
		});
		onResume();
	}

	@Override
	public void onDestroy() {
		//TODO dispose Timer here...
	}

	@Override
	public void onResume() {
		messageService.getMessages(null, fromId, new AsyncCallback<List<Message>>() {
			
			@Override
			public void onSuccess(List<Message> result) {
				//Set this data to the list...
				messageListProvider.getList().clear();
				messageListProvider.getList().addAll(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error while receiving messages!");
			}
		});
	}

}
