package com.samsung.gwttraining.client.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.samsung.gwttraining.client.AuthService;
import com.samsung.gwttraining.client.AuthServiceAsync;
import com.samsung.gwttraining.client.events.EventBus;
import com.samsung.gwttraining.client.events.EventType;

public class LogView extends Composite implements WidgetLifecycle {
	
	private static final AuthServiceAsync authService = (AuthServiceAsync)GWT.create(AuthService.class);

	private static LogViewUiBinder uiBinder = GWT.create(LogViewUiBinder.class);

	interface LogViewUiBinder extends UiBinder<Widget, LogView> {
	}
	
	@UiField
	DockLayoutPanel logDockPanel;
	
	@UiField
	VerticalPanel logPanel;
	
	@UiField
	TextBox login;
	
	@UiField
	Button logButton;
	
	@UiHandler("logButton")
	void logButtonClick(ClickEvent e) {
		final String nickName = login.getValue();
		if(nickName.isEmpty()) {
			Window.alert("Fill login field!");
			return;
		}
		authService.log(nickName, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				EventBus.INSTANCE.dispatch(EventType.LOGIN, nickName);
				return;
			}
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Login is already used, or another errors occured!");
			}
		});
	}

	public LogView() {
		initWidget(uiBinder.createAndBindUi(this));
		onCreate();
	}

	@Override
	public void onCreate() {
	}

	@Override
	public void onDestroy() {
		login.setText("");
	}

	@Override
	public void onResume() {
	}

}
