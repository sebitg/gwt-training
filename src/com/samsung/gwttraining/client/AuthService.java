package com.samsung.gwttraining.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.samsung.gwttraining.shared.pojos.User;

@RemoteServiceRelativePath("auth")
public interface AuthService extends RemoteService {
	
	void log(String userName);
	
	//TODO!!!
	//Write method that checks nickname (if it is currently used in system)
	
	/**
	 * Check, if nickname is available
	 * @param userName
	 */
	Boolean checkNick(String userName);
	
	List<User> getUsersOnline();
	
	Boolean logOut();
	
	String isSomeoneLogged();

}
