package com.samsung.gwttraining.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.samsung.gwttraining.shared.pojos.Message;

/**
 * Message service
 * @author mody
 *
 */
@RemoteServiceRelativePath("message")
public interface MessageService extends RemoteService {

	Boolean sendMessage(Integer toId, Integer fromId, String message);
	
	List<Message> getMessages(Integer toId, Integer fromId);
	
	Boolean checkNewMessages(Integer toId, Integer fromId);
	
}
