package com.samsung.gwttraining.server;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Hibernate Helper
 * @author mody
 *
 */
public class HibernateUtil {
	
	private static final Logger LOG = Logger.getLogger(HibernateUtil.class.toString());
	
	private static SessionFactory sessionFactory;
	
	public static SessionFactory getSessionFactory() {
		if(sessionFactory==null) {
			try {
				Configuration configuration = new Configuration().configure();
				ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
			            configuration.getProperties()).buildServiceRegistry();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			}
			catch(Exception e) {
				LOG.log(Level.SEVERE, "Error while Session Factory init. Msg: " + e.getMessage());
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}

}
