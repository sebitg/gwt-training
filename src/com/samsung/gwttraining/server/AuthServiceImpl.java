package com.samsung.gwttraining.server;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.samsung.gwttraining.client.AuthService;
import com.samsung.gwttraining.shared.pojos.User;

public class AuthServiceImpl extends RemoteServiceServlet implements AuthService {
	
	private static final String SESSION_USER = "userName";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@Override
	public void log(String userName) {
		if(checkNick(userName)) {
			Session currentSession = sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			User user = new User();
			user.setUserName(userName);
			currentSession.save(user);
			currentSession.getTransaction().commit();
			HttpSession httpSession = this.getThreadLocalRequest().getSession();
			httpSession.setAttribute(SESSION_USER, userName);
			return;
		}
		throw new IllegalArgumentException("Given nick is busy!");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUsersOnline() {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		Query createQuery = currentSession.createQuery("from User");
		List<User> list = (List<User>)createQuery.list();
		currentSession.getTransaction().commit();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Boolean checkNick(String userName) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		Query query = currentSession.createQuery("from User as u where u.userName = :userName");
		query.setParameter("userName", userName);
		List<User> list = (List<User>)query.list();
		currentSession.getTransaction().commit();
		return list.isEmpty();
	}

	@Override
	public Boolean logOut() {
		//Remove currently logged user and clean session data
		HttpSession httpSession = this.getThreadLocalRequest().getSession();
		String userName = (String)httpSession.getAttribute(SESSION_USER);
		
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		Query query = currentSession.createQuery("from User as u where u.userName = :userName");
		query.setParameter("userName", userName);
		User user = (User)query.list().get(0);
		currentSession.delete(user);
		currentSession.getTransaction().commit();
		
		httpSession.removeAttribute(SESSION_USER);
		return true;
	}

	@Override
	public String isSomeoneLogged() {
		HttpSession httpSession = this.getThreadLocalRequest().getSession();
		String userName = (String)httpSession.getAttribute(SESSION_USER);
		
		return userName;
	}

}
