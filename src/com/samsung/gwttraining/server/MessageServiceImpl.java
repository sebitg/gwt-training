package com.samsung.gwttraining.server;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.samsung.gwttraining.client.MessageService;
import com.samsung.gwttraining.shared.pojos.Message;
import com.samsung.gwttraining.shared.pojos.User;

public class MessageServiceImpl extends RemoteServiceServlet implements
		MessageService {
	
	private static final String SESSION_USER = "userName";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@Override
	public Boolean sendMessage(Integer toId, Integer fromId, String message) {
		if(fromId==null) {
			fromId = getUserBySession().getId();
		}
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		Message msg = new Message();
		msg.setFromId(fromId);
		msg.setToId(toId);
		msg.setMsg("<b>" + getCurrentUserName() + "</b>: " + message);
		currentSession.save(msg);
		currentSession.getTransaction().commit();
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getMessages(Integer toId, Integer fromId) {
		StringBuilder queryString = new StringBuilder();
		if(toId==null && fromId==null) {	//Global chat
			queryString.append("from Message as m where m.toId=NULL");
		}
		else if(fromId!=null) {
			toId = getUserBySession().getId();
			queryString.append("from Message as m where (m.toId=:toId and m.fromId=:fromId) or (m.fromId=:toId and m.toId=:fromId)");
		}
		
		if(queryString.length()!=0) {
			Session currentSession = sessionFactory.getCurrentSession();
			currentSession.beginTransaction();
			
			Query query = currentSession.createQuery(queryString.toString());
			if(fromId!=null) {
				query.setParameter("fromId", fromId);
				query.setParameter("toId", toId);
			}
			List<Message> messages = (List<Message>)query.list();
			currentSession.getTransaction().commit();
			return messages;
		}
		throw new IllegalArgumentException("Wrong combination of toId/fromId args!");
	}

	@Override
	public Boolean checkNewMessages(Integer toId, Integer fromId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected User getUserBySession() {
		String fromIdAsString = getCurrentUserName();
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		Query query = currentSession.createQuery("from User as u where u.userName = :userName");
		query.setParameter("userName", fromIdAsString);
		User user = (User)query.list().get(0);
		currentSession.getTransaction().commit();
		return user;
	}
	
	protected String getCurrentUserName() {
		HttpSession httpSession = this.getThreadLocalRequest().getSession();
		String fromIdAsString = (String)httpSession.getAttribute(SESSION_USER);
		return fromIdAsString;
	}

}
